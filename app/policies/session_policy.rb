class SessionPolicy < ApplicationPolicy
  def sign_in?
    user.nil?
  end

  def create?
    user.nil?
  end

  def logout?
    user.present?
  end

  def reset_password?
    user.nil?
  end

  def signed_in?
    user.present?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end

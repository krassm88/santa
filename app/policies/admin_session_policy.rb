class AdminSessionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def current_user_is_admin?
    user.present? && user.has_role?(:admin)
  end
end

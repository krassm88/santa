$(document).on('turbolinks:load', function() {
  $('#letter_images').on('cocoon:after-insert', function() {
    preview_image();
    check_to_hide_or_show_add_link();
  });

  $('#letter_images').on('cocoon:after-remove', function() {
    check_to_hide_or_show_add_link();
  });

  check_to_hide_or_show_add_link();

  function check_to_hide_or_show_add_link() {
    if ($('#letter_images .nested-fields:visible').length == 3) {
      $('#letter_images .links a').hide();
    } else {
      $('#letter_images .links a').show();
    }
  }

  function preview_image() {
    $('.letter_image').change(function () {
      readURL(this)
    })
  }

  $('.letter_image').change(function () {
    readURL(this)
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(input).parent('.field').children('.preview_image').html("<img class='ui small image' src='" + e.target.result + "'/>");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
});
// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require turbolinks
//= require jquery.slick
//= require jquery.inputmask
//= require jquery.inputmask.extensions
//= require jquery.inputmask.numeric.extensions
//= require jquery.inputmask.date.extensions
//= require slick
//= require semantic-ui
//= require sidebar
//= require tabs
//= require questions
//= require vk
//= require selectize
//= require cocoon
//= require letters

// Anchor Links
function jump(h){
  var top = document.getElementById(h).offsetTop;
  ///window.scrollTo(0, top)
  $("html, body").animate({scrollTop: top }, 600);
}

// Calculate Order Price without Delivery
totalOrderPrice = function() {
  var totalPrice = 0;

  $.each($('#new_order .checkbox input[type="checkbox"]'), function(index, item) {
    var checkbox = $(item).parent();

    if ($(item)[0].hasAttribute('checked')) checkbox.addClass('checked');

    if (checkbox.hasClass('checked')) totalPrice += $(item).data('price');
  });

  // Add totalPrice to Hidden Field
  if ($('#order_total_price').length > 0) document.getElementById('order_total_price').value = totalPrice;

  // Fill TotalPrice
  $('#total-price').html(totalPrice);

  return totalPrice;
};

// Calculate Order price with delivery
deliveryPrice = function() {
  var totalPrice = totalOrderPrice() + $('#delivery_price').data('price');

  // Add totalPrice to Hidden Field
  if ($('#order_total_price').length > 0) document.getElementById('order_total_price').value = totalPrice;

  // Fill TotalPrice
  $('#total-price').html(totalPrice);
};



$(document).on('turbolinks:load', function() {

  // Fill total price with default price
  totalOrderPrice();

  // Semantic UI
  $('.ui.checkbox').checkbox();
  $('.ui.dropdown').dropdown();
  $('.popup').popup();

  // Mask for phone
  var tel = document.getElementById("tel");
  $(tel).inputmask('+7(999) 999-9999');

  // Fill total price
  $('#new_order .checkbox.checkbox_price').on('click', function() {

    // Get value of Order Price
    var totalPrice = parseInt(document.getElementById('order_total_price').value);

    if ($(this).hasClass('checked')) {
      $(this).addClass('checked');
      totalPrice += $(this).find('input').data('price');
    } else {
      $(this).removeClass('checked');
      totalPrice -= $(this).find('input').data('price');
    }

    // Add totalPrice to Hidden Field
    if ($('#order_total_price').length > 0) document.getElementById('order_total_price').value = totalPrice;

    $('#total-price').html(totalPrice);
  });

  // Calculate Delivery Price
  $('.delivery_type_checkbox').click(function() {
    var checkboxId = $(this).attr('id');

    switch (checkboxId) {
      case 'self_checkbox':
        totalOrderPrice();
        break;
      case 'away_city_checkbox':
        deliveryPrice();
        break;
      case 'in_city_checkbox':
        deliveryPrice();
        break;
    }
  });

  // Selectize remove corners after focus (only on letter page)
  $('.letter_city').focusin(function() {
    $('.selectize-input').addClass('rectangle_border')
  }).focusout(function() {
    $('.selectize-input').removeClass('rectangle_border')
  });

  $('#letter_city_id').selectize({
    valueField: 'id',
    labelField: 'title',
    searchField: 'title',
    create: function(input, callback) {
      $.ajax({
        url: '/cities',
        type: 'POST',
        dataType: 'JSON',
        data: {
          city: {
            title: input
          }
        },
        success: function(city) {
          callback(city);
        },
        error: function() {
          callback();
        }
      })
    }
  });

  $('#order_customer_email').on('focusout', function(el) {
    $.ajax({
      url: '/users',
      type: 'POST',
      dataType: 'json',
      data: {
        user: {
          email: el.target.value
        }
      },
      success: function(){},
      error: function(){}
    })
  })
});

// Hide Buttons on first and last pages
switchButtons = function (firstButton, secondButton) {
  $(firstButton).hide();
  $(secondButton).show()
}

// Set Speed of SnowFlake Line
animateTab = function(point) {
  $('#snowflake').animate({'left': point + '%'}, 10);
  $('.active_tab_nav').animate({'left': (point - 100.0) + '%'}, 10);
}

// Set directions of SnowFlake Blue line
progressBar = function(start, end) {
  // Get min and max values
  var minValue = Math.min(start, end);
  var maxValue = Math.max(start, end);

  if (minValue == start) {
    while (minValue < maxValue) {
      minValue += 1;
      animateTab(minValue)
    }
  } else {
    while (minValue < maxValue) {
      maxValue -= 1;
      animateTab(maxValue)
    }
  }
}

// Activate checkbox
activateCheckbox = function(checkbox, deliveryType) {

  $(checkbox).show(200, function(){
    $(checkbox + ' input').removeAttr('checked');

    $('.delivery_type_checkbox').removeClass('checked');

    //$('.delivery_address').hide();
    if (checkbox.includes(deliveryType)) {
      $(checkbox.match(deliveryType) +  '_address').show();
      $(checkbox).click();
    }
  });

}


$(document).on('turbolinks:load', function() {
  // Hide 'Previous Button'
  $('#prev_tab').hide();

  // Tab buttons
  $('#tabs').tabs();

  //Select Order Type (buttons with icons)
  $('.letter_type').click(function() {
    $('.hidden_image').hide();
    $('.visible_image').show();

    //Remember for_whom_letter
    document.getElementById('order_for_whom_letter').value = $(this).find('h4').text();

    // Hide unselected order types
    $('.letter_type').addClass('letter_disabled');
    $(this).removeClass('letter_disabled');

    // Show tick
    $(this).find('.visible_image').hide();
    $(this).find('.hidden_image').show();
  });

  // Show Text Area when select checkbox
  $('.delivery_type_checkbox').click(function() {

    if (!$(this).hasClass('checked')) {
      var checkboxId = $(this).attr('id');

      $('.delivery_address').hide();

      switch (checkboxId) {
        case 'self_checkbox':
          $('#self_address').show();
          document.getElementById('order_delivery_type').value = "self";

          break;
        case 'out_city_checkbox':
          $('#out_city_address').show('fast', function(){
            $('#out_city_address .order_form').focus();
            document.getElementById('order_delivery_type').value = "out_city";
          });

          break;
        case 'in_city_checkbox':
          $('#in_city_address').show('fast', function(){
            $('#in_city_address .order_form').focus();
            document.getElementById('order_delivery_type').value = "in_city";
          });

          break;
      }
    }
  });

  // Semantic UI Modal
  $('#your_region, .region_link').click(function( event ) {
    event.preventDefault();
    $('.ui.modal').modal('show');
  });

  $('.hide_modal').click(function() {
    $('.ui.modal').modal('hide');
  });


  $('.city_checkbox').on('click', function() {

    // Hide text_areas of address and checkboxes of delivery addresses
    $('.delivery_address, .delivery_type_checkbox').hide();

    // Fill your region
    $('#your_region').html($(this).find('label').text());

    // Get City Id
    var cityId = $(this).find('input').attr('value');
    var deliveryType = document.getElementById('order_delivery_type').value;

    // Remember City Id
    document.getElementById('order_city_id').value = cityId;

    // Get Delivery Points
    $.getJSON( rootUrl + 'cities/' + cityId + '/delivery_addresses.json', function( data ) {

      var deliveryAddressesCount = data.length;

      // Show self delivery Points
      if (deliveryAddressesCount != 0) {
        var deliveryAddressesHtml = "<select name='order[delivery_address_id]' id='delivery_address_id' class='ui fluid dropdown order_form'>";

        activateCheckbox('#self_checkbox', deliveryType);

        // for (let i of [0, deliveryAddressesCount - 1]) {
        //   deliveryAddressesHtml += '<option value=' + data[i]['id'] + '>' + data[i]['address'] + '</option>'
        // }
        var i = 0;

        while (i < deliveryAddressesCount) {
          deliveryAddressesHtml += '<option value=' + data[i]['id'] + '>' + data[i]['address'] + '</option>';
          i++;
        }
        deliveryAddressesHtml += '</select>';

        document.getElementById('self_address').innerHTML = deliveryAddressesHtml;

        $('#delivery_address_id').dropdown();
      }
    });

    // Fill delivery prices
    $.getJSON( rootUrl + 'cities/' + cityId + '.json', function( data ) {
      if (data['delivery_price'] > 0) {
        activateCheckbox('#in_city_checkbox', deliveryType);
        $('#in_city_checkbox').find('label').html('Доставка по городу - ' +
                                                  '<span id="delivery_price" data-price=' + data['delivery_price'] +
                                                  '>' + data['delivery_price'] + '</span>' + ' руб.');

        if (data['km_price'] > 0) {
          activateCheckbox('#out_city_checkbox', deliveryType);
          $('#out_city_checkbox').find('label').html('Доставка за город - ' + data['delivery_price'] + ' руб. + ' +
                                                      data['km_price'] + ' руб./км');
        }
      }
    });
  });

  // Switch Tab Buttons
  $('#prev_tab, #next_tab, .tab_href').click(function() {

    var tabId;

    if ($(this).hasClass('tab_href')) {
      // Get visible tabId
      tabId = parseInt($(this).attr('href').split('_')[1])

    } else {
      // Get visible tabId
      tabId = parseInt($('.tab:visible').attr('id').split('_')[1]) + parseInt(this.value);

      // Get next tabId
      nextTab = document.getElementById('tab_' + tabId);

      // Hide all tabs
      $('.tab').hide();

      // Show next tab
      $(nextTab).show()
    }

    // Switch tab nav link
    $('li').removeClass('ui-tabs-active ui-state-active');
    $('a[href="#tab_' + tabId + '"]').parent().addClass('ui-tabs-active ui-state-active');

    // SnowFlake points
    var totalWidth = parseFloat($(".progressBar").css('width').split('px')[0]);
    var startWidth = parseFloat($('#snowflake').css('left').split('px')[0]);

    progressBar(100 * startWidth/totalWidth, 12.3 * (2 * tabId - 1));

    // Switch buttons
    if (tabId == 1) {

      switchButtons('#prev_tab', '#next_tab')

    } else if (tabId == 2) {

      $('#prev_tab, #next_tab').show();

      // Get ForWhomLetter
      var orderWhomLetter = document.getElementById('order_for_whom_letter').value;

      if (orderWhomLetter) {
        $.each($('button.letter_type'), function(index, item) {
          if ($(item).find('h4').text() == orderWhomLetter) {
            $(item).click();
            return false;
          }
        });
      }

    } else if (tabId == 3) {

      $('#prev_tab, #next_tab').show();

    } else if (tabId == 4) {

      switchButtons('#next_tab', '#prev_tab');

      // Get cityId
      var cityId = document.getElementById('order_city_id').value;

      // Click on checkbox of same city
      if (cityId) {
        $.each($('.city_checkbox input[type="radio"]'), function(index, item) {
          if ($(item).data('city-id') == cityId) {
            $(item).parent().click();
            return false;
          }
        })
      } else {
        // Activate Default City
        $('.city_checkbox').first().click();
      }

    }
  })

  // Fill order_user_address from textareas
  $('#submitBtn').on('click', function() {
    var checkedBox = $('.delivery_type_checkbox.checked').first();
    var addressId = $(checkedBox).attr('id').replace('_checkbox','_address');
    var userAddress = document.getElementById('order_user_address');

    if ($(checkedBox).attr('id') != 'self_checkbox') {
      userAddress.value = document.getElementById(addressId).children[0].value;
    } else {
      userAddress.value = $('#' + addressId + ' .dropdown .text').text();
    }
  })

})

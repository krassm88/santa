//= require rails-ujs
//= require jquery
//= require jquery_ujs

//= require paper-dashboard/js/bootstrap.min
//= require paper-dashboard/js/bootstrap-checkbox-radio
//= require paper-dashboard/js/chartist.min
//= require paper-dashboard/js/bootstrap-notify
//= require paper-dashboard/js/paper-dashboard

//= require cocoon
//= require dropzone
//= require ./dropzone_upload

//= require libs/mask/mask.min

$(document).on('ready', function() {
  $('#order_customer_phone').mask("+7(999) 999-9999");
})

valueIsExist = function(element) {
  return typeof element !== 'undefined' && element;
}

valuesAreExist = function(element1, element2) {
  return valueIsExist($(element1).val()) && valueIsExist($(element2).val())
}

simpleFormat = function(str) {
  str = str.replace(/\r\n?/, "\n");
  str = $.trim(str);
  if (str.length > 0) {
    str = str.replace(/\n\n+/g, '</p><p>');
    str = str.replace(/\n/g, '<br />');
    str = '<p>' + str + '</p>';
  }
  return str;
}

$(document).on('change', '#text_template_id', function(e){

  var textTemplateId = $(this).val();

  if (valueIsExist(textTemplateId)) {

    // Fill textarea of congratulation
    $.getJSON( rootUrl + 'lk/letter_text_templates/' + textTemplateId + '.json', function( data ) {
      document.getElementById('text_form').innerHTML = data['text'];
    });

  } else {
    // Empty textarea of congratulation
    document.getElementById('text_form').innerHTML = '';
  }
});


// Show Preview Button if all fields selected
$(document).on('change', '#text_template_id, #image_template_id', function(e){
 (valuesAreExist('#image_template_id', '#text_template_id')) ? $('#preview_button').show() : $('#preview_button').hide();
});


$(document).on('click', '#preview_button', function(e){

  nameOfCongratulation = document.getElementById('name_of_congratulation').value;

  if (valueIsExist(nameOfCongratulation)) document.getElementById('modal_header').innerHTML = "<h4 class='modal-title'>" + nameOfCongratulation + "</h4>";

  $.getJSON( rootUrl + 'lk/letter_image_templates/' + document.getElementById('image_template_id').value + '.json', function( data ) {
    document.getElementById('preview_body').innerHTML = "<img src='" + data['url'] +
                                                        "' class='background_congratulation'><div class='text_congratulation'><p>" +
                                                        simpleFormat(document.getElementById('text_form').value) + "</p></div>"
  });
});

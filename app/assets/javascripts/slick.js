runCarousel = function () {

  $('.next_arrow').on('click', function() {
    $('.carousel').slick('slickNext');
  });

  $('.prev_arrow').on('click', function() {
    $('.carousel').slick('slickPrev');
  });

  $('.carousel').not('.slick-initialized').slick({
    //centerMode: true,
    arrows: false,
    lazyLoad: 'ondemand',
    //variableWidth: true
    slidesToShow: 4,
    slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: false
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
  });
};

$(document).on('turbolinks:load', runCarousel);

//Now it will not throw error, even if called multiple times.
$(window).on('resize', runCarousel);

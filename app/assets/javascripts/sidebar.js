toggleLeftSidebar = function () {
  if ($('#left-sidebar').length) $('.ui.sidebar').first().sidebar('attach events', '#left-sidebar');
};

hideSidebar = function () {
  if ($('#left-sidebar').length) $('.ui.sidebar').first().sidebar('hide')
};


$(document).on('turbolinks:load', toggleLeftSidebar);

$(window).on('resize', hideSidebar);

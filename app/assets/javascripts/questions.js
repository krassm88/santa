// Rotate plus icon
$.fn.rotate = function(degrees, step, current) {
    var self = $(this);
    current = current || 0;
    step = step || 5;
    current += step;
    self.css({
        '-webkit-transform' : 'rotate(' + current + 'deg)',
        '-moz-transform' : 'rotate(' + current + 'deg)',
        '-ms-transform' : 'rotate(' + current + 'deg)',
        'transform' : 'rotate(' + current + 'deg)'
    });
    if (current != degrees) {
        setTimeout(function() {
            self.rotate(degrees, step, current);
        }, 5);
    }
}


$(document).on('turbolinks:load', function() {

  // Questions
  $(document).on('click','.get_answer', function(event) {
    event.preventDefault();

    var questionId = $(this).data('id');

    var answer = document.getElementById("answer_" + questionId);

    if ($(answer).is(':visible')) {
      $('#button_' + questionId).rotate(90);
      $(answer).hide(500);
    } else {
      $(answer).show(500);
      $('#button_' + questionId).rotate(45);
    }
  })
})

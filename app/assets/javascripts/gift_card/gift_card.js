//= require jquery.inputmask.js

$(function() {
	$(document).ready(function () {
		$('.icon-cancel').on('click', function () {
			$('.feedback-container').css('display', 'none')
		});
		$('.btn-order').on('click', function () {
			$('.feedback-container').css('display', 'block')
		})
	})
});

$(document).ready(function() {
    $('.additional-gifts__continer').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        prevArrow:'.arrow__left',
        nextArrow:'.arrow__right'
    });
    $(".class_nav").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
    });

    $('.hamb').on('click', function () {
        $(this).toggleClass('hamb-close')
        $('.nav-container').toggleClass('nav-container__activ animated fadeInLeftBig')
    })
    $('.issues-accordion__title').on('click', function(){
        $(this).find('.issues-accordion__pluse').toggleClass('active-arrow');
        var content = $(this).siblings();

        if(content.is(':visible')){
            content.slideUp();
        } else {
            content.slideDown();
        }
    });

    $('.face-btn').hover(function () {
        $(this).toggleClass('animated tada');
    });
    var face = $('div').is('.face-conatiner'),
        anime = $('div').is('.first-anim');
    if (face) {
        var poinerContainer = $('.face-conatiner').offset().top
        $('.face-conatiner').mousemove(function (e) {
            var pointX = e.pageY;

            if (pointX >= poinerContainer + 260){
                $('.eye-dot').addClass('dote-anime');
            } else {
                $('.eye-dot').removeClass('dote-anime');
            }

            if(pointX >= poinerContainer + 200){
                $('.face-smile').empty().append('<div class="img-smile"><img src="../assets/svg/laughing.svg" alt=""></div>')
            } else {
                $('.face-smile').empty().append('<div class="face-mouth"></div>')
            }

            if (pointX <= poinerContainer + 80) {
                $('.eye-dot').css('top','15px');
                $('.eye-left__dot').css('right','15px');
                $('.eye-right__dot').css('left','15px');
                $('.eye-dot').addClass('anim-first');
            } else {
                $('.eye-dot').removeClass('anim-first');
            }
            if (pointX >= poinerContainer + 100) {
                $('.eye-dot').css('top','23px');
                $('.eye-left__dot').css('right','13px');
                $('.eye-right__dot').css('left','17px');
            }
            if (pointX >= poinerContainer + 130) {
                $('.eye-dot').css('top','26px');
                $('.eye-left__dot').css('right','11px');
                $('.eye-right__dot').css('left','19px');
            }
            if (pointX >= poinerContainer + 160) {
                $('.eye-dot').css('top','29px');
                $('.eye-left__dot').css('right','9px');
                $('.eye-right__dot').css('left','21px');
            }
            if (pointX >= poinerContainer + 190) {
                $('.eye-dot').css('top','34px');
                $('.eye-left__dot').css('right','7px');
                $('.eye-right__dot').css('left','23px');
            }
            if (pointX >= poinerContainer + 220) {
                $('.eye-dot').css('top','39px');
                $('.eye-left__dot').css('right','9px');
                $('.eye-right__dot').css('left','21px');
            }
            if (pointX >= poinerContainer + 250) {
                $('.eye-dot').css('top','45px');
                $('.eye-left__dot').css('right','11px');
                $('.eye-right__dot').css('left','19px');
            }
        });
    }

    if (anime) {
        $(window).scroll(function () {
            var firstAnim= $('.first-anim').offset().top - 400,
                lastAnim= $('.last-anim').offset().top - 400,
                secondAnim= $('.second-anim').offset().top - 400,
                scrollEfect = $(window).scrollTop();
            if(scrollEfect > firstAnim) {
                $('.first-anim').css('visibility', 'visible').addClass("animated fadeInRightBig");
            }
            if(scrollEfect > lastAnim) {
                $('.last-anim').css('visibility', 'visible').addClass("animated fadeInRightBig");
            }
            if(scrollEfect > secondAnim) {
                $('.second-anim').css('visibility', 'visible').addClass("animated fadeInLeftBig");
            }
        });
    }




});



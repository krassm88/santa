class Letter < ApplicationRecord
  has_many :letter_images
  accepts_nested_attributes_for :letter_images, allow_destroy: true, reject_if: :all_blank

  validates :age, inclusion: { in: 1..100 }
  validates :email, email: true, presence: true
  validates :text, length: { minimum: 20 }
end

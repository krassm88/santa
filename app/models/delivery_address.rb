class DeliveryAddress < ApplicationRecord
  belongs_to :city
  has_many :orders

  validates :address, presence: true
end

class Congratulation < ApplicationRecord
  belongs_to :letter_text_template
  belongs_to :letter_image_template

  validates_presence_of :letter_text_template_id, :letter_image_template_id

  validates :name, presence: true, uniqueness: true

  validates :text, presence: true, length: { in: 20..10000 }
end

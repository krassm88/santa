class Faq < ApplicationRecord
  validates :answer, :question, presence: true
end

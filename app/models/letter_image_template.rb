class LetterImageTemplate < ApplicationRecord
  has_many :congratulations

  validates :name, presence: true, uniqueness: true

  mount_uploader :background_image, LetterImageTemplateUploader
end

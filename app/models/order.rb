class Order < ApplicationRecord
  FOR_WHOM_LETTER_LIST = ['Мальчик не умеет читать', 'Мальчик', 'Мужчина', 'Девочка не умеет читать', 'Девочка', 'Женщина', 'Группа людей']
  DELIVERY_TYPE_LIST = ['self', 'in_city', 'out_city']

  FOR_WHOM_LETTER = [
                       { image: 'img/svg/symbol/sprite.svg#girl_cant_read', title: 'Девочка не умеет читать', class: 'checks-elements__pink' },
                       { image: 'img/svg/symbol/sprite.svg#boy_cant_read', title: 'Мальчик не умеет читать', class: 'checks-elements__blue' },
                       { image: 'img/svg/symbol/sprite.svg#boy', title: 'Мальчик', class: 'checks-elements__blues' },
                       { image: 'img/svg/symbol/sprite.svg#girl', title: 'Девочка', class: 'checks-elements__red' },
                       { image: 'img/svg/symbol/sprite.svg#man', title: 'Мужчина', class: 'checks-elements__fiol' },
                       { image: 'img/svg/symbol/sprite.svg#woman', title: 'Женщина', class: 'checks-elements__maroon' },
                       { image: 'img/svg/symbol/sprite.svg#group', title: 'Группа людей', class: 'checks-elements__yellow' }
                    ]

  belongs_to :city
  belongs_to :delivery_address, optional: true
  belongs_to :user, optional: true
  has_and_belongs_to_many :gifts
  accepts_nested_attributes_for :gifts, allow_destroy: true

  validates :customer_email,  email: true, allow_blank: true
  validates :customer_phone,  presence: true
  validates :delivery_type,   presence: true, inclusion: { in: DELIVERY_TYPE_LIST }
  validates :for_whom_letter, presence: true, inclusion: { in: FOR_WHOM_LETTER_LIST }
  validates :names,           presence: true
  validates :total_price,     presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :user_address,    length: { in: 5..1000 }, allow_blank: true

  after_create :order_accepted
  before_validation do
    uniq_gifts =  if gifts.any?
                    (gifts + Gift.includes_in_order).uniq
                  else
                    Gift.includes_in_order
                  end    
    self.gifts = uniq_gifts

    total_sum = self.gifts.sum(&:new_price)
    if %w(in_city out_city).include?(delivery_type)
      total_sum += city.delivery_price 
      self[:delivery_address_id] = nil 
    end
    self[:total_price] = total_sum.to_i
  end

  private

  def order_accepted
    OrderMailer.order_accepted(self).deliver_later if customer_email.present?
  end
end

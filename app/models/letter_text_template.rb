class LetterTextTemplate < ApplicationRecord
  has_many :congratulations

  validates :name, presence: true, uniqueness: true

  validates :text, presence: true, uniqueness: true, length: { in: 20..10000 }
end

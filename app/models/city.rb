class City < ApplicationRecord
  has_many :delivery_addresses, dependent: :destroy
  accepts_nested_attributes_for :delivery_addresses, reject_if: :all_blank, allow_destroy: true

  validates :title, presence: true, uniqueness: true

  validates :delivery_price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :km_price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end

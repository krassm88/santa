class LetterImage < ApplicationRecord
  belongs_to :letter, optional: true

  validates :image, file_size: { less_than_or_equal_to: 5.megabytes },
            file_content_type: { allow: ['image/jpeg', 'image/png'] }

  mount_uploader :image, LetterImageUploader
end

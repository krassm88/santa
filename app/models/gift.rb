class Gift < ApplicationRecord
  scope :includes_in_order, -> () { where(include_in_order: true) }
  scope :not_includes_in_order, -> () { where(include_in_order: false) }

  has_and_belongs_to_many :orders

  validates :name, presence: true, uniqueness: true

  mount_uploader :avatar, GiftUploader
end

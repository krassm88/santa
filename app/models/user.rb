class User < ApplicationRecord
  has_secure_password
  rolify

  scope :newest, -> () { order(created_at: :desc) }

  has_many :orders

  validates :email, email: true, allow_blank: true, uniqueness: true
  validates :password, length: { minimum: 6 }
  validates_confirmation_of :password
  validates :username, presence: true, uniqueness: true

  before_validation :generate_username, if: -> { username.nil? }

  class << self
    def generate_password
      rand(100000..999999).to_s
    end
  end

  private

  def generate_username
    return unless EmailValidator.valid?(email)
    login = email.split('@')[0]

    i = 0
    loop do
      new_username = i == 0 ? login : "#{login}_#{i}"
      unless User.find_by(username: new_username)
        (self[:username] = new_username) and break
      end
      i += 1
    end
  end
end

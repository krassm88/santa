class LettersController < ApplicationController
  before_action :load_cities

  def new
    @letters = Letter.where(published: true).order(created_at: :desc)
  end

  def create
    @letter = Letter.new(letter_params)
    if @letter.save
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def letter_params
    params.require(:letter).permit(:name, :email, :city, :age, :text, :published,
                                   letter_images_attributes: [:id, :image, :_destroy, :image_cache])
  end

  def load_cities
    @cities = City.all
  end
end
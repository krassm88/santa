class CitiesController < ApplicationController
  def create
    @city = City.create(city_params)
    if @city.persisted?
      render json: @city
    else
      render json: { errors: @city.errors }
    end
  end

  def show
    @city = City.find(params[:id])
    respond_to do |format|
      format.json {render json: @city}
      format.js
    end
  end

  private

  def city_params
    params.require(:city).permit(:title)
  end
end

class SessionsController < ApplicationController
  before_action :authorize_pundit

  def sign_in;end

  def create
    warden.authenticate!(:password, action: :sign_in)
    redirect_to (current_user.has_role?(:admin) ? lk_root_path : cabinet_root_path)
  end

  def logout
    warden.logout
    redirect_to root_path
  end

  def reset_password
    if request.post?
      @user = User.find_by(email: params[:email])
      if @user
        @password = User.generate_password
        @user.update(password: @password)
        UserMailer.reset_password(@user, @password).deliver_later
      else
        @message = 'Такой email не найден'
      end
      respond_to do |format|
        format.js {}
      end
    end
  end

  private

  def authorize_pundit
    authorize :session
  end
end
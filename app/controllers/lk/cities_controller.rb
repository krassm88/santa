class Lk::CitiesController < ::Lk::BaseController
  before_action :load_city, only: [:edit, :update]

  def index
    @q = City.ransack(params[:q])
    @cities = @q.result.page(params[:page]).per(20)
  end

  def new
  end

  def create
    @city = City.create(city_params)
    @city.persisted? ? (redirect_to edit_lk_city_path(@city)) : (render :new)
  end

  def edit
  end

  def update
    @city.update(city_params) ? (redirect_to edit_lk_city_path(@city)) : (render :edit)
  end

  private

  def city_params
    params.require(:city).permit(:title, :description, :delivery_price, :km_price, 
      delivery_addresses_attributes: [:id, :address, :tech_description, :schedule, :_destroy])
  end

  def load_city
    @city = City.find(params[:id])
  end
end

class Lk::LettersController < ::Lk::BaseController
  def index
    @q = Letter.ransack(params[:q])
    @letters = @q.result.order(created_at: :desc).page(params[:page]).per(20)
  end

  def show
    @letter = Letter.find(params[:id])
  end
end
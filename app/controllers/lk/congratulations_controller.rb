class Lk::CongratulationsController < ::Lk::BaseController
  before_action :load_congratulation, only: [:edit, :update, :destroy, :show]

  def index
    @q = Congratulation.order(created_at: :desc).ransack(params[:q])
    @congratulations = @q.result.page(params[:page]).per(20)
  end

  def new
    @congratulation = Congratulation.new
  end

  def create
    @congratulation = Congratulation.new(congratulation_params)
    @congratulation.save ? (redirect_to lk_congratulations_path) : (render :new)
  end

  def edit
  end

  def update
    @congratulation.update(congratulation_params) ? (redirect_to lk_congratulations_path) : (render :edit)
  end

  def destroy
    @congratulation.destroy
    redirect_to lk_congratulations_path
  end

  def show
    respond_to do |format|
      format.pdf do
        pdf_file_name = [@congratulation.id, @congratulation.name].join('_')
        @pdf = render_to_string pdf: pdf_file_name, template: 'lk/congratulations/show.html.slim', encoding: 'utf8'
        send_data(@pdf, filename: pdf_file_name, type: 'application/pdf')
      end
    end
  end

  private

  def congratulation_params
    params.require(:congratulation).permit(:name, :text, :letter_image_template_id, :letter_text_template_id)
  end

  def load_congratulation
    @congratulation = Congratulation.find(params[:id])
  end
end

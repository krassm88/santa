class Lk::GalleriesController < ::Lk::BaseController
  def index
    @images = Gallery.order(order_field: :asc)
  end

  def create
    @image = Gallery.create(
      image: params[:file]
    )
    render json: @image
  end

  def update
    @image = Gallery.find(params[:id])
    @image.update(gallery_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @image = Gallery.find(params[:id])
    @image.destroy
    respond_to do |format|
      format.js
    end
  end

  private

  def gallery_params
     params.require(:gallery).permit(:order_field)   
  end
end
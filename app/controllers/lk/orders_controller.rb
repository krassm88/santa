class Lk::OrdersController < ::Lk::BaseController
  before_action :load_order, only: [:edit, :update, :show, :destroy]

  def index
    @q = Order.order(created_at: :desc).ransack(params[:q])
    @orders = @q.result.page(params[:page]).per(20)
  end

  def show;end

  def edit;end

  def update
    if @order.update(order_params)
      redirect_to lk_order_path(@order)
    else
      render :edit
    end
  end

  def destroy
    @order.destroy and (redirect_to lk_orders_path)
  end

  private

  def order_params
    params.require(:order).permit(:for_whom_letter, :names, :note, :address_on_the_envelope, 
                                  :customer_name, :customer_phone, :customer_email, :city_id, 
                                  :delivery_address_id, :total_price, :user_address, :delivery_type, 
                                  gift_ids: [])
  end

  def load_order
    @order = Order.find(params[:id])
  end
end
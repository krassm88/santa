class Lk::CallbackPhonesController < ::Lk::BaseController
  def index
    @q = CallbackPhone.ransack(params[:q])
    @callback_phones = @q.result.page(params[:page]).per(25)    
  end
end
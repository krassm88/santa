class Lk::StaticDataController < ::Lk::BaseController
  def index
    @settings = StaticData.get_all
  end

  def update
    config_params.each do |k, v|
      StaticData[k.to_sym] = v
    end
    redirect_to lk_static_data_path, notice: t('updated')
  end

  private

  def config_params
    params.permit(:email, :msk_phone, :spb_phone)
  end
end
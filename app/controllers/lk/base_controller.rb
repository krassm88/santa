class Lk::BaseController < ApplicationController
  layout 'lk/layouts/application'

  before_action :authorize_pundit

  private

  def authorize_pundit
    authorize :admin_session, :current_user_is_admin?
  end
end

class Lk::GiftsController < ::Lk::BaseController
  before_action :load_gift, only: [:edit, :update, :destroy]

  def index
    @q = Gift.order(created_at: :desc).ransack(params[:q])
    @gifts = @q.result.page(params[:page]).per(20)
  end

  def new
    @gift = Gift.new
  end

  def create
    @gift = Gift.new(gift_params)
    @gift.save ? (redirect_to lk_gifts_path) : (render :new)
  end

  def edit
  end

  def update
    @gift.update(gift_params) ? (redirect_to lk_gifts_path) : (render :edit)
  end

  def destroy
    @gift.destroy
    redirect_to lk_gifts_path
  end

  private

  def gift_params
    params.require(:gift).permit(:name, :old_price, :new_price, :avatar, :avatar_cache, :yandex_market,
                                 :on_root_page, :include_in_order, :description, :priority)
  end

  def load_gift
    @gift = Gift.find(params[:id])
  end
end
class Lk::FaqsController < ::Lk::BaseController
  before_action :load_faq, only: [:edit, :update, :destroy]

  def index
    @q = Faq.order(number_sort: :asc).ransack(params[:q])
    @faqs = @q.result.page(params[:page]).per(20)
  end

  def create
    @faq = Faq.new(faq_params)
    if @faq.save
      redirect_to edit_lk_faq_path(@faq), notice: t('created')
    else
      render :new
    end
  end

  def update
    if @faq.update(faq_params)
      redirect_to edit_lk_faq_path(@faq), notice: t('updated')
    else
      render :edit, alert: t('not_updated')
    end
  end

  def destroy
    @faq.destroy
    redirect_to lk_faqs_path, notice: t('destroyed')
  end

  private

  def faq_params
    params.require(:faq).permit(:question, :answer, :number_sort)
  end

  def load_faq
    @faq = Faq.find(params[:id])
  end
end
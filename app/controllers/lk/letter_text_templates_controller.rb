class Lk::LetterTextTemplatesController < ::Lk::BaseController
  before_action :load_letter_text_template, only: [:show, :edit, :update, :destroy]

  def index
    @q = LetterTextTemplate.order(created_at: :desc).ransack(params[:q])
    
    @letter_text_templates = @q.result.page(params[:page]).per(20)
  end

  def new
    @letter_text_template = LetterTextTemplate.new
  end

  def create
    @letter_text_template = LetterTextTemplate.new(letter_text_template_params)

    @letter_text_template.save ? (redirect_to lk_letter_text_templates_path) : (render :new)
  end

  def edit
  end

  def update
    @letter_text_template.update(letter_text_template_params) ? (redirect_to lk_letter_text_templates_path) : (render :edit)
  end

  def destroy
    @letter_text_template.destroy

    redirect_to lk_letter_text_templates
  end

  def show
    respond_to do |format|
      format.json
      format.js
    end
  end

  private

  def letter_text_template_params
    params.require(:letter_text_template).permit(:name, :text)
  end

  def load_letter_text_template
    @letter_text_template = LetterTextTemplate.find(params[:id])
  end
end
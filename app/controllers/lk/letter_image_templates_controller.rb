class Lk::LetterImageTemplatesController < ::Lk::BaseController
  before_action :load_letter_image_template, only: [:show, :edit, :update, :destroy]

  def index
    @q = LetterImageTemplate.order(created_at: :desc).ransack(params[:q])
    
    @letter_image_templates = @q.result.page(params[:page]).per(20)
  end

  def new
    @letter_image_template = LetterImageTemplate.new
  end

  def create
    @letter_image_template = LetterImageTemplate.new(letter_image_template_params)

    @letter_image_template.save ? (redirect_to lk_letter_image_templates_path) : (render :new)
  end

  def edit
  end

  def update
    @letter_image_template.update(letter_image_template_params) ? (redirect_to lk_letter_image_templates_path) : (render :edit)
  end

  def destroy
    @letter_image_template.destroy

    redirect_to lk_letter_image_templates
  end

  def show
    respond_to do |format|
      format.json
      format.js
    end
  end

  private

  def letter_image_template_params
    params.require(:letter_image_template).permit(:name, :background_image)
  end

  def load_letter_image_template
    @letter_image_template = LetterImageTemplate.find(params[:id])
  end
end
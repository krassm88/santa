class Lk::UsersController < ::Lk::BaseController
  def index
    @q = User.without_role(:admin).newest.ransack(params[:q])
    @users = @q.result.includes(:orders).page(params[:page]).per(20)
  end
end
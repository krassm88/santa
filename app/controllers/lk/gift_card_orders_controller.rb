class Lk::GiftCardOrdersController < ::Lk::BaseController
  def index
    @q = GiftCardOrder.order(created_at: :desc).ransack(params[:q])
    @gift_card_orders = @q.result.page(params[:page]).per(20)
  end
end
class GiftCardOrdersController < ApplicationController
  def create
    @gift_card_order = GiftCardOrder.create(gift_card_order_params)
    respond_to do |format|
      format.js {}
    end
  end

  private

  def gift_card_order_params
    params.require(:gift_card_order).permit(:fio, :email, :phone)
  end
end
class Api::CitiesController < ::Api::BaseController
  def index
    @cities = City.all
    respond_to do |format|
      format.json
    end
  end
end
class Api::DeliveryAddressesController < ::Api::BaseController
  before_action :load_city

  def index
    @delivery_addresses = @city.delivery_addresses
    respond_to do |format|
      format.json
    end
  end

  private

  def load_city
    @city = City.find(params[:city_id])
  end
end
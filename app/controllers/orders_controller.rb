class OrdersController < ApplicationController
  layout 'base'

  before_action :load_cities
  before_action :load_user, only: [:create]

  def new
  end

  def create
    @order = if @user.present?
               @user.orders.create(order_params)
             else
               Order.create(order_params)
             end
    @order.persisted? ? (render template: 'orders/thank_for_order', layout: 'base')   : (render :new)
  end

  private

  def order_params
    params.require(:order).permit(:for_whom_letter, :names, :note, :address_on_the_envelope, :customer_name,
                                  :customer_phone, :customer_email, :city_id, :delivery_address_id, :total_price, :user_address, :delivery_type, gift_ids: [])
  end

  def load_cities
    @default_city = City.includes(:delivery_addresses).where(title: 'Санкт-Петербург').first || City.includes(:delivery_addresses).first 
    @cities = City.all.order(title: :asc)
  end

  def load_user
    @user = User.find_by(email: order_params[:customer_email])
  end
end

class HomeController < ApplicationController
  layout :load_layout


  def index
    @gifts_on_root_page = Gift.where(on_root_page: true).order(priority: :desc)
    @faqs = Faq.order(number_sort: :asc).all
    @min_price = Gift.where(include_in_order: true).sum(:new_price)
    @gallery = Gallery.order(order_field: :asc)
  end

  def catalog
  end

  private

  def load_layout
    case action_name
      when 'index' then 'base'
      else false
    end
  end
end
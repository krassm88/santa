class DeliveryAddressesController < ApplicationController
  before_action :load_city

  def index
    @delivery_addresses = @city.delivery_addresses

    respond_to do |format|
      format.json {render json: @delivery_addresses}
      format.js
    end
  end

  private

  def load_city
    @city = City.find(params[:city_id])
  end
end

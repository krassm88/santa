class StaticPagesController < ApplicationController
  layout 'layouts/custom', only: [:thank_for_order]
  
  def home
    @gifts_on_root_page = Gift.where(on_root_page: true).order(priority: :desc)
    @faqs = Faq.order(number_sort: :asc).all
  end

  def letter
  end

  def thank_for_order
  end

  def robots
    respond_to :text
    expires_in 6.hours, public: true
  end
end

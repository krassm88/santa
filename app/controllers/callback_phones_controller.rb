class CallbackPhonesController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create

  def create
    callback = CallbackPhone.create(callback_phone_params)
    render json: callback
  end

  private

  def callback_phone_params
    params.require(:callback_phone).permit(:name, :phone)
  end
end
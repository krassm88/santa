class Cabinet::OrdersController < ::Cabinet::BaseController
  def index
    @q = current_user.orders.order(created_at: :desc).includes(:gifts).ransack(params[:q])
    @orders = @q.result
  end

  def show
    @order = Order.find(params[:id])
  end
end
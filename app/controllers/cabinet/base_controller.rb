class Cabinet::BaseController < ApplicationController
  before_action :authorize_pundit

  layout 'layouts/cabinet'

  private

  def authorize_pundit
    authorize :session, :signed_in?
  end
end
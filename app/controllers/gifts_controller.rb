class GiftsController < ApplicationController
  layout 'layouts/gift_card_layout'

  def show
    @gift = Gift.find_by!(id: params[:id], yandex_market: true)
  end
end
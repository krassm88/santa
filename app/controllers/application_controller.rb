class ApplicationController < ActionController::Base
  include AuthHelper
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :render_404
  rescue_from ActionController::RoutingError, with: :render_404
  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  protect_from_forgery with: :exception

  helper_method :current_user, :warden

  def render_404
    render template: 'errors/error404', layout: 'base'
  end
end

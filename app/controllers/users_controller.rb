class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create

  def create
    @user = User.find_or_initialize_by(user_params)
    if @user.persisted?
      render json: @user
    else
      password = User.generate_password
      @user.tap do |user|
        user.password = password
        user.password_confirmation = password
      end

      @user.save ? (render json: @user, status: :created and UserMailer.cabinet_created(@user, password).deliver_later) : (render json: { errors: @user.errors }, status: :unprocessable_entity)
    end
  end

  private

  def user_params
    params.require(:user).permit(:email)
  end
end
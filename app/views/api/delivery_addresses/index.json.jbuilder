json.array! @delivery_addresses do |delivery_address|
  json.id delivery_address.id
  json.address delivery_address.address
  json.city_id delivery_address.city_id
end

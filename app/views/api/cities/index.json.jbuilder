json.array! @cities do |city|
  json.id city.id
  json.title city.title
  json.description city.description
  json.delivery_price city.delivery_price
  json.km_price city.km_price
end
class OrderMailer < ApplicationMailer

  def order_accepted(order)
    @order = order
    mail(to: order.customer_email, subject: "Ваш заказ принят")
  end
end

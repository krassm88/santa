class UserMailer < ApplicationMailer
  def cabinet_created(user, password)
    @user = user
    @password = password
    mail(to: @user.email, subject: "Ваш кабинет создан")
  end

  def reset_password(user, password)
    @user = user
    @password = password
    mail(to: @user.email, subject: "Ваш новый пароль")
  end
end

class ApplicationMailer < ActionMailer::Base
  default from: 'info@f121.ru'
  layout 'mailer'
end

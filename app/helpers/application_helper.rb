module ApplicationHelper
  def cabinet_active_link(path, controllers = nil)
    current_path = if controllers.present?
                     if controllers.is_a?(Array)
                       controllers.include?(controller_path)
                     else
                       controller_path == controllers
                     end
                   else
                     request.fullpath == path
                   end

    klass = { class: 'item' }
    current_path ? klass[:class] << ' active' : nil
    link_to path, klass do
      yield
    end
  end

  def default_meta_tags
    {
        description: 'Сделайте новогоднюю ночь 2018 незабываемой, оставив на память необыновенный подарок!',
        keywords:    'Письмо деду морозу, 2018',
        separator:   "&mdash;".html_safe,
        site: t('title'),
        reverse: true
    }
  end

  def humanize_boolean(attr)
    attr ? t('yes') : t('no')
  end

  def humanize_delivery_type(attr)
    case attr
    when 'self' then t('delivery_types.self')
    when 'in_city' then t('delivery_types.in_city')
    when 'out_city' then t('delivery_types.out_city')
    else ''
    end
  end

  def humanize_price(price)
    price == 0 ? 'Бесплатно' : "#{price} руб."
  end
end

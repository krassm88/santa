module ::Lk::ApplicationHelper
  def sidebar_active_link(path, controllers = nil)
    current_path = if controllers.present?
                     if controllers.is_a?(Array)
                       controllers.include?(controller_path)
                     else
                       controller_path == controllers
                     end
                   else
                     request.fullpath == path
                   end

    klass = current_path ? { class: 'active' } : nil
    content_tag(:li, klass) do
      link_to path do
        yield
      end
    end
  end
end
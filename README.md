# Установка

Ruby version: 2.4.2

Устновить библиотеки: 

```optipng jpegoptim pngquant```

Перед стартом, скопировать config/database.yml.sample в config/database.yml и выполнить:
```
rake db:create 
rake db:migrate 
rake db:seed
```

Для получение email и фоновых задач запустить:
```
QUEUE=* rake resque:work
``` 

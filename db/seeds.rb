# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Rails.env.development?
  Gift.create(name: 'Медведь Тедди', old_price: 350, new_price: 210, avatar: File.open(File.join(Rails.root, '/db/fixtures/bear.png')))
  Gift.create(name: 'Сладкий подарок', avatar: File.open(File.join(Rails.root, '/db/fixtures/present.png')))
  Gift.create(name: 'Дед мороз', old_price: 190, new_price: 170, avatar: File.open(File.join(Rails.root, '/db/fixtures/santa.png')))
  Gift.create(name: 'Спиннер', old_price: 150, new_price: 130, avatar: File.open(File.join(Rails.root, '/db/fixtures/spinner.png')))

  admin = User.create(username: 'admin', password: 'qwerty123', password_confirmation: 'qwerty123')
  admin.add_role(:admin)

  City.create(title: "Москва", delivery_price: 200, km_price: 20)
end

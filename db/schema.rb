# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171211211820) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "callback_phones", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "delivery_price", default: 0
    t.integer "km_price", default: 0
    t.index ["title"], name: "index_cities_on_title", unique: true
  end

  create_table "congratulations", force: :cascade do |t|
    t.string "name"
    t.text "text"
    t.bigint "letter_image_template_id"
    t.bigint "letter_text_template_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["letter_image_template_id"], name: "index_congratulations_on_letter_image_template_id"
    t.index ["letter_text_template_id"], name: "index_congratulations_on_letter_text_template_id"
  end

  create_table "delivery_addresses", force: :cascade do |t|
    t.string "address"
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "tech_description", default: ""
    t.text "schedule", default: ""
    t.index ["city_id"], name: "index_delivery_addresses_on_city_id"
  end

  create_table "faqs", force: :cascade do |t|
    t.string "question", default: ""
    t.string "answer", default: ""
    t.integer "number_sort", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "galleries", force: :cascade do |t|
    t.string "image"
    t.integer "order_field", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gift_card_orders", force: :cascade do |t|
    t.string "fio"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gifts", force: :cascade do |t|
    t.string "name"
    t.string "avatar"
    t.integer "old_price", default: 0, null: false
    t.integer "new_price", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "on_root_page", default: false
    t.boolean "include_in_order", default: false
    t.text "description", default: ""
    t.boolean "priority", default: false
    t.boolean "yandex_market", default: false
  end

  create_table "gifts_orders", force: :cascade do |t|
    t.bigint "gift_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gift_id"], name: "index_gifts_orders_on_gift_id"
    t.index ["order_id"], name: "index_gifts_orders_on_order_id"
  end

  create_table "letter_image_templates", force: :cascade do |t|
    t.string "name"
    t.string "background_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "letter_images", force: :cascade do |t|
    t.string "image"
    t.bigint "letter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["letter_id"], name: "index_letter_images_on_letter_id"
  end

  create_table "letter_text_templates", force: :cascade do |t|
    t.string "name"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "letters", force: :cascade do |t|
    t.string "name", default: ""
    t.integer "age"
    t.string "email"
    t.text "text", default: ""
    t.boolean "published", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "city"
  end

  create_table "orders", force: :cascade do |t|
    t.string "for_whom_letter"
    t.string "names"
    t.text "note"
    t.text "address_on_the_envelope"
    t.string "customer_name"
    t.string "customer_phone"
    t.string "customer_email"
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "delivery_address_id"
    t.bigint "user_id"
    t.integer "total_price"
    t.string "delivery_type"
    t.text "user_address"
    t.index ["city_id"], name: "index_orders_on_city_id"
    t.index ["delivery_address_id"], name: "index_orders_on_delivery_address_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "delivery_addresses", "cities"
end

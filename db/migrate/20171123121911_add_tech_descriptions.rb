class AddTechDescriptions < ActiveRecord::Migration[5.1]
  def change
    add_column :delivery_addresses, :tech_description, :text, default: ''
  end
end

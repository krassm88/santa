class AddYandexMarketBooleanToGifts < ActiveRecord::Migration[5.1]
  def change
    add_column :gifts, :yandex_market, :boolean, default: false
  end
end

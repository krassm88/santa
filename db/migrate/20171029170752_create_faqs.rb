class CreateFaqs < ActiveRecord::Migration[5.1]
  def change
    create_table :faqs do |t|
      t.string :question, default: ''
      t.string :answer, default: ''
      t.integer :number_sort, default: 0

      t.timestamps
    end
  end
end

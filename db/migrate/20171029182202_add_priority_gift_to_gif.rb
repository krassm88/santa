class AddPriorityGiftToGif < ActiveRecord::Migration[5.1]
  def change
    add_column :gifts, :priority, :boolean, default: false
  end
end

class AddDescriptionToGift < ActiveRecord::Migration[5.1]
  def change
    add_column :gifts, :description, :text, default: ''
  end
end

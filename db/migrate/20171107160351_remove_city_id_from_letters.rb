class RemoveCityIdFromLetters < ActiveRecord::Migration[5.1]
  def change
    remove_column :letters, :city_id
    add_column :letters, :city, :string
  end
end

class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :for_whom_letter
      t.string :names
      t.text :note
      t.text :address_on_the_envelope
      t.string :customer_name
      t.string :customer_phone
      t.string :customer_email
      t.belongs_to :city, index: true

      t.timestamps
    end
  end
end

class AddToOrdersBelongsToDeliveryAddress < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :delivery_address, index: true
  end
end

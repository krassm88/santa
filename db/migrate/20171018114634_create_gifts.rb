class CreateGifts < ActiveRecord::Migration[5.1]
  def change
    create_table :gifts do |t|
      t.string :name
      t.string :avatar
      t.integer :old_price, null: false, default: 0
      t.integer :new_price, null: false, default: 0

      t.timestamps
    end
  end
end

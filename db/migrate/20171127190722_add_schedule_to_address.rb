class AddScheduleToAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :delivery_addresses, :schedule, :text, default: ''
  end
end

class CreateCongratulations < ActiveRecord::Migration[5.1]
  def change
    create_table :congratulations do |t|
      t.string :name
      t.text :text
      t.references :letter_image_template, index: true
      t.references :letter_text_template, index: true
      t.timestamps
    end
  end
end

class CreateLetterTextTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :letter_text_templates do |t|
      t.string :name
      t.text :text

      t.timestamps
    end
  end
end

class CreateDeliveryAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_addresses do |t|
      t.string :address
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end

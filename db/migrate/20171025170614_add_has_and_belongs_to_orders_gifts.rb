class AddHasAndBelongsToOrdersGifts < ActiveRecord::Migration[5.1]
  def change
    create_table :gifts_orders do |t|
      t.belongs_to :gift, index: true
      t.belongs_to :order, index: true

      t.timestamps
    end
  end
end

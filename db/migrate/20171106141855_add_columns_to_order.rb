class AddColumnsToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :delivery_type, :string
    add_column :orders, :user_address, :text
  end
end

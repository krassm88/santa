class AddCheckboxesForGifts < ActiveRecord::Migration[5.1]
  def change
    add_column :gifts, :on_root_page, :boolean, default: false
    add_column :gifts, :include_in_order, :boolean, default: false
  end
end

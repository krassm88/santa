class CreateCallbackPhones < ActiveRecord::Migration[5.1]
  def change
    create_table :callback_phones do |t|
      t.string :name
      t.string :phone

      t.timestamps
    end
  end
end

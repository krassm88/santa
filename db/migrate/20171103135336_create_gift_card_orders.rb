class CreateGiftCardOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :gift_card_orders do |t|
      t.string :fio
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end

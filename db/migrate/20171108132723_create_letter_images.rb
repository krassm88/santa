class CreateLetterImages < ActiveRecord::Migration[5.1]
  def change
    create_table :letter_images do |t|
      t.string :image
      t.references :letter, index: true

      t.timestamps
    end
  end
end

class CreateLetterImageTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :letter_image_templates do |t|
      t.string :name
      t.string :background_image

      t.timestamps
    end
  end
end

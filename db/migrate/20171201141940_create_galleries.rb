class CreateGalleries < ActiveRecord::Migration[5.1]
  def change
    create_table :galleries do |t|
      t.string :image
      t.integer :order_field, default: 0

      t.timestamps
    end
  end
end

class AddColumnsToCity < ActiveRecord::Migration[5.1]
  def change
    add_column :cities, :delivery_price, :integer, default: 0
    add_column :cities, :km_price, :integer, default: 0
  end
end

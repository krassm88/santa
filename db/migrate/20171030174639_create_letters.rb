class CreateLetters < ActiveRecord::Migration[5.1]
  def change
    create_table :letters do |t|
      t.belongs_to :city, index: true
      t.string :name, default: ''
      t.integer :age, default: ''
      t.string :email
      t.text :text, default: ''
      t.boolean :published, default: false

      t.timestamps
    end
  end
end

# Warden::Strategies.add(:admin_password) do
#   def valid?
#     params['login'].present? && params['password'].present?
#   end
#
#   def authenticate!
#     user = User.find_by(username: params['login'])
#     if user&.has_role?(:admin) && user.authenticate(params['password'])
#       success! user
#     else
#       fail I18n.t('invalid_login_or_password')
#     end
#   end
# end

Warden::Strategies.add(:password) do
  def valid?
    params['login'].present? && params['password'].present?
  end

  def authenticate!
    user = User.find_by(username: params['login']) || User.find_by(email: params['login'])
    if user && user.authenticate(params['password'])
      success! user
    else
      fail I18n.t('invalid_login_or_password')
    end
  end
end
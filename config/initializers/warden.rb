Rails.application.config.middleware.use Warden::Manager do |manager|
  manager.default_strategies :password
  manager.failure_app = lambda { |env|
    failure_action = env["warden.options"][:action].to_sym
    SessionsController.action(failure_action ? failure_action : :sign_in).call(env)
  }
end

Warden::Manager.serialize_into_session do |user|
  user.id
end

Warden::Manager.serialize_from_session do |id|
  User.find(id)
end
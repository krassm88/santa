WickedPdf.config = {
  layout: 'layouts/application.pdf',
  margin: { top:    3,
            bottom: 0,
            left:   0,
            right:  0 }
}

require 'resque_web'

Rails.application.routes.draw do
  get '/robots.:format' => 'static_pages#robots'

  namespace :cabinet do
    resources :orders, only: [:show]

    root to: 'orders#index'
  end

  namespace :lk do
    mount ResqueWeb::Engine => "/resque"

    resources :callback_phones, only: :index
    resources :cities, except: [:show]
    resources :congratulations
    resources :galleries, only: [:index, :create, :update, :destroy]
    resources :gift_card_orders, only: :index
    resources :gifts, except: :show
    resources :faqs, except: :show
    resources :letters, only: [:index, :show]
    resources :letter_image_templates
    resources :letter_text_templates
    resources :orders, except: [:new, :create]
    resources :static_data, only: [:index] do
      patch :update, on: :collection
    end
    resources :users, only: [:index]

    root to: 'dashboard#index'
  end

  namespace :api do
    resources :cities, only: [:index] do
      resources :delivery_addresses, only: [:index]
    end
  end

  resources :callback_phones, only: :create

  resources :cities, only: [:create, :show] do
    resources :delivery_addresses, only: [:index]
  end
  resources :gifts, only: :show
  # resources :gift_card_orders, only: :create
  get :catalog, to: 'home#catalog'

  resources :orders, only: [:create]
  get :order, to: 'orders#new'
  resources :users, only: [:create]

  get :napisat_pismo_dedu_morozu_v_velikiy_ustyug_2018, to: 'letters#new'
  post :napisat_pismo_dedu_morozu_v_velikiy_ustyug_2018, to: 'letters#create'

  get 'napisat_pismo_dedu_morozu_v_velikiy_ustyug_2018', to: 'letters#new'
  post 'napisat_pismo_dedu_morozu_v_velikiy_ustyug_2018', to: 'letters#create'

  match :reset_password, to: 'sessions#reset_password', via: [:get, :post]
  get :sign_in, to: 'sessions#sign_in'
  post :sign_in, to: 'sessions#create'
  delete :logout, to: 'sessions#logout'

  root to: 'home#index'
  # root to: 'static_pages#home'
  # get 'thank_for_order', to: 'static_pages#thank_for_order'

  get '*unmatched_route', to: 'application#render_404'
end

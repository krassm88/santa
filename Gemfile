source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'bcrypt'

gem 'carrierwave'
gem 'carrierwave-imageoptimizer'
gem 'cocoon'
gem 'coffee-rails', '~> 4.2'

gem 'dropzonejs-rails'

gem 'email_validator'

gem 'file_validators'
gem 'font-awesome-rails'

gem 'inline_svg'

gem 'jbuilder', '~> 2.5'
gem 'jquery-inputmask-rails'
gem 'jquery-rails'
gem 'jquery-slick-rails'
gem 'jquery-ui-rails'

gem 'kaminari'
gem 'kaminari-i18n'

gem 'meta-tags'
gem 'mini_magick'

gem 'pg'
gem 'puma', '~> 3.7'
gem 'pundit'

gem 'rails', '~> 5.1.4'
gem 'rails-i18n'
gem 'rails-settings-cached'
gem 'ransack', github: 'activerecord-hackery/ransack'
gem 'rack-cors'
gem 'resque'
gem 'resque-web', require: 'resque_web'
gem 'rolify'

gem 'sass-rails', '~> 5.0'
gem 'selectize-rails'
gem 'sitemap_generator'
gem 'slim-rails'
gem 'sinatra', require: false
gem 'semantic-ui-sass', git: 'https://github.com/doabit/semantic-ui-sass.git'

gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'uglifier', '>= 1.3.0'
gem 'unicorn'

gem 'warden'
gem 'whenever', require: false
gem 'wicked_pdf', '~> 1.1.0'
gem 'wkhtmltopdf-binary'

group :development do
  gem 'bullet'

  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-passenger'
  gem 'capistrano-rvm'

  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.2'

  gem 'sqlite3'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  gem 'capybara', '~> 2.13'

  gem 'selenium-webdriver'

  gem 'pry-rails'
end
